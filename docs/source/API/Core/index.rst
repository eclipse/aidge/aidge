Aidge core API
==============


.. toctree::
    :maxdepth: 1

    data.rst
    graph.rst
    operator.rst
    backend.rst
    scheduler.rst
    graphMatching.rst
    recipes.rst
    export.rst

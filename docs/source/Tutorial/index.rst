Tutorials
=========

Aidge 101
---------

To get started with Aidge, please follow the Aidge demonstration tutorial.
This tutorial demonstrates the basic features of the Aidge Framework, importing an ONNX, transforming a neural network graph, performing inference and a cpp export.

.. nbgallery::
    101_first_step.nblink


Aidge DNN fonctionnalities
--------------------------

- Manipulating databases and creating batches of data
- Train a Deep Neural Network
- Provide an operator implementation using Python or meta-operators
- Perform advanced graph matching with the Graph Regular Expression tool

.. nbgallery::
    database.nblink
    graph_matching.nblink
    scheduling.nblink
    learning.nblink
    ONNX.nblink

DNN Optimization
----------------

- Perform post Training Quantization
- Perform Convolution tiling

.. nbgallery::
    ptq.nblink
    tiling.nblink

DNN export
----------

.. nbgallery::
    export_cpp.nblink

- `Exercise on adding a custom implementation for a cpp export <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/main/examples/tutorials/export/aidge_to_cpp/add_custom_operator/add_custom_operator.ipynb?ref_type=heads>`_
- `Export your DNN with TensorRT <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/main/examples/tutorials/TRT_Quantization_tutorial/tuto.ipynb?ref_type=heads>`_
- `Export your DNN for an STM32 <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/main/examples/tutorials/export/aidge_to_STM32/export_LeNet/export_lenet_fp32.ipynb?ref_type=heads>`_

Tutorial on adding the C++ Aidge
--------------------------------

You can extend our operator coverage by adding an operator and its implementation in the C++ Aidge library.
The `Add an operator and its implementation Tutorial <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/main/examples/tutorials/Hardmax_operator/operator_adding.ipynb?ref_type=heads>`_ details the steps to follow.

For more information on contributing to Aidge, please refer to the `wiki <https://gitlab.eclipse.org/groups/eclipse/aidge/-/wikis/Contributing>`_.

If you encounter any difficulty with the Tutorials, please create an issue `here <https://gitlab.eclipse.org/groups/eclipse/aidge/-/issues>`_.

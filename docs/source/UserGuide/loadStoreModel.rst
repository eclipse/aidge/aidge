Load and store model
====================

ONNX
----

Load model from ONNX
^^^^^^^^^^^^^^^^^^^^

AIDGE allows loading a DNN model stored in ONNX format. 
The resulting :ref:`graph view <source/userguide/modelGraph:Graph View>` is created by: 

* Converting all ONNX initializers to Producers.
* Converting all ONNX node to :ref:`nodes <source/userguide/modelGraph:Node>` with the corresponding :ref:`Operator <source/userguide/modelGraph:Operator>`, based on the operator's name. If an operator is unknown, a :ref:`generic operator <source/userguide/modelGraph:Generic operator>` is used.
* Adding all the necessary connections between all the nodes of the :ref:`computational graph <source/userguide/modelGraph:Introduction>`.

The feature recquires a dependency to protobuff and at least a backend (in order to store the weights). This feature is available via a Python plugin.



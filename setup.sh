#!/usr/bin/env bash
# Aidge Project Build Script with Dynamic Compiler Selection

set -euo pipefail
# Exit immediately if a command exits with a non-zero status (error)
# Treat unset variables as an error when substituting
# Return value of a pipeline is the value of the last (rightmost) command to exit with a non-zero status


# Determine the operating system
OS=$(uname -s)

# Default compiler and number of core selection based on OS
case "$OS" in
    Darwin)
        DEFAULT_C_COMPILER=$(command -v clang || echo "gcc")
        DEFAULT_CXX_COMPILER=$(command -v clang++ || echo "g++")
        NUM_CORES=$(sysctl -n hw.ncpu)
        ;;
    Linux)
        DEFAULT_C_COMPILER=$(command -v gcc || echo "cc")
        DEFAULT_CXX_COMPILER=$(command -v g++ || echo "c++")
        NUM_CORES=$(nproc)
        ;;
    *)
        DEFAULT_C_COMPILER="cc"
        DEFAULT_CXX_COMPILER="c++"
        NUM_CORES=1
        ;;
esac

# Map architecture and handle MacOS-specific flags
ARCH=$(uname -m)
CMAKE_OSX_ARCH=""
if [ "$OS" = "Darwin" ]; then
    CMAKE_OSX_ARCH="-DCMAKE_OSX_ARCHITECTURES=${ARCH}"
fi

# Colors and font options
readonly BOLD='\033[1m'
readonly UNDERLINE='\033[4m'
readonly RED='\033[31m'
readonly GREEN='\033[32m'
readonly YELLOW='\033[33m'
readonly BLUE='\033[34m'
readonly RESET='\033[0m' # Font reset

# Logging functions
log_debug() {
    printf "[${GREEN}${BOLD}DEBUG${RESET}] - %s\n" "$*"
}
log_info() {
    printf "[${BLUE}${BOLD}INFO${RESET}] - %b\n" "$1"
}

log_warning() {
    printf "[${BOLD}${YELLOW}WARNING${RESET}] %b\n" "$1"
}

log_error() {
    printf "[${BOLD}${RED}ERROR${RESET}] %s\n" "$*" >&2
}


# Script options
OPT_BUILD_MODE="Debug"
AIDGE_INSTALL_PREFIX="${AIDGE_INSTALL:-$(pwd)/aidge/install}"
ENABLE_TESTS="OFF"
PYTHON_BUILD="false"
OPT_WITH_PYBIND="OFF"
OPT_WITH_CUDA="OFF"
MODULE_SELECTION="default"
OPT_ASAN="OFF"
OPT_C_COMPILER="$DEFAULT_C_COMPILER"
OPT_CXX_COMPILER="$DEFAULT_CXX_COMPILER"
nb_proc=$((${NUM_CORES} > 1 ? ${NUM_CORES} - 1 : 1))


# Module dependency order
ORDERED_MODULES=(
    "aidge_core"
    "aidge_backend_cpu"
    "aidge_backend_opencv"
    "aidge_backend_cuda"
    "aidge_onnx"
    "aidge_quantization"
    "aidge_export_cpp"
    "aidge_export_arm_cortexm"
    "aidge_learning"
)

# Default modules
DEFAULT_MODULES=(
    "aidge_core"
    "aidge_backend_cpu"
    "aidge_onnx"
    "aidge_quantization"
    "aidge_export_cpp"
)

# Initialize an empty array to store the directory names
custom_module_list=()
add_to_unique_list() {
    local arg=$1
    if [[ (${#custom_module_list[@]} -eq 0) || (! " ${custom_module_list[@]} " =~ " aidge_$arg ") ]]; then
        if [[ -d "aidge/aidge_$arg" ]]; then
            custom_module_list+=("aidge_$arg")
            if [[ ! " ${ORDERED_MODULES[@]} " =~ " aidge_$arg " ]]; then
                ORDERED_MODULES+=("aidge_$arg")
            fi

        else
            log_warning "Cannot find directory $(pwd)/aidge/${BOLD}aidge_${arg}${RESET}"
        fi
    fi
}

print_help() {
    cat << EOF
Build script for the Aidge project.

Args:
    -p | --python
    -b | --pybind        Generate python bindings
    -t | --tests         Build and run tests
    -m | --module  -m    Select modules to build. Modules are aidge repos without "aidge_" prefix
    -a                   Build all modules
    -s | --asan          Enable Address SANitizer
                         WARNING : ASAN & gdb don't work together. Starting a debugging session will result in undefined behavior
    -j                   Specify the number of cores to build the projects, if not specified will be ${nb_proc}
    --debug              Build in Debug mode [DEFAULT BUILD MODE]
    --release            Build in Release mode
    --prefix PATH        Set installation prefix (default: $(pwd)/aidge/install)
    -h |--help           Print this message
    --cc COMPILER        Set C compiler (default: ${DEFAULT_C_COMPILER})
    --cxx COMPILER       Set C++ compiler (default: ${DEFAULT_CXX_COMPILER})

Detected OS: ${OS}
EOF
}

POSITIONAL_ARGS=()

# Parse command-line arguments
parse_arguments() {
    while [[ $# -gt 0 ]]; do
        case $1 in
            -p|--python)
                PYTHON_BUILD="true"
                shift # past argument
                ;;
            -b|--pybind)
                OPT_WITH_PYBIND="ON"
                shift # past argument
                ;;
            -t|--tests)
                ENABLE_TESTS="ON"
                shift # past argument
                ;;
            --cuda)
                OPT_WITH_CUDA="ON"
                shift # past argument
                ;;
            -m|--module)
                MODULE_SELECTION="custom"
                add_to_unique_list "$2"
                shift # past argument
                shift # past value
                ;;
            -a|--all)
                MODULE_SELECTION="all"
                shift # past argument
                ;;
            --debug)
                OPT_BUILD_MODE="Debug"
                shift # past argument
                ;;
            --release)
                OPT_BUILD_MODE="Release"
                shift # past argument
                ;;
            -s|--asan)
                OPT_ASAN="ON"
                echo "OPT_ASAN = $OPT_ASAN"
                shift # past argument
                ;;
            -j)
                nb_proc=$2
                shift # past argument
                shift # past value
                ;;
            --prefix)
                AIDGE_INSTALL_PREFIX="$2"
                shift 2
                ;;
            --cc)
                OPT_C_COMPILER="$2"
                shift 2
                ;;
            --cxx)
                OPT_CXX_COMPILER="$2"
                shift 2
                ;;
            -h|--help)
                print_help
                exit
                ;;
            -*|--*)
                echo "Unknown option $1"
                exit 1
                ;;
            *)
                POSITIONAL_ARGS+=("$1") # save positional arg
                shift # past argument
                ;;
        esac
    done

    # Ensure POSITIONAL_ARGS is not unbound
    POSITIONAL_ARGS=${POSITIONAL_ARGS[@]+"${POSITIONAL_ARGS[@]}"}
}

if [[ "$nb_proc" ]]; then
    re='^[0-9]+$'
        if ! [[ "$nb_proc" =~ $re ]] ; then
            log_error "nb processor specified is not a number" >&2; exit 1
    fi
fi


################################
# get the list of available modules
detect_modules() {
    local available_modules=()
    while IFS= read -r -d '' dir; do
        name="${dir#aidge/}"
        available_modules+=("$name")
    done < <(find aidge -maxdepth 1 -type d -name "aidge_*" -print0)

    echo "${available_modules[@]}"
}
################################

# Main build function
build_aidge() {
    parse_arguments "$@"
    export AIDGE_INSTALL=${AIDGE_INSTALL_PREFIX}

    local modules_to_build=()
    case "$MODULE_SELECTION" in
        "all")
            modules_to_build=($(detect_modules))
            ;;
        "default")
            modules_to_build=("${DEFAULT_MODULES[@]}")
            [[ "$OPT_WITH_CUDA" == "ON" ]] && modules_to_build+=("aidge_backend_cuda")
            ;;
        "custom")
            modules_to_build=("${custom_module_list[@]}")
            ;;
    esac

    mkdir -p "${AIDGE_INSTALL_PREFIX}"
    log_info "Installing Aidge library at ${AIDGE_INSTALL_PREFIX}"

    for module in "${ORDERED_MODULES[@]}"; do
        if [[ " ${modules_to_build[*]} " =~ " $module " ]]; then
            build_module "$module"
        else
            log_info "Skipping module: ${module} - not specified"
        fi
    done
}

# Module build sub-function with OS-specific handling
build_module() {
    local module="$1"
    log_info "Compiling module: ${BOLD}${module}${RESET}"

    if [[ "$PYTHON_BUILD" == "true" ]]; then
        build_python_module "$module"
    else
        build_cpp_module "$module"
    fi
}

build_python_module() {
    local module="$1"
    if [[ -f "aidge/${module}/setup.py" || -f "aidge/${module}/pyproject.toml" ]]; then

        # Set environment variables for CMake
        export AIDGE_C_COMPILER="${OPT_C_COMPILER}"
        export AIDGE_CXX_COMPILER="${OPT_CXX_COMPILER}"
        export AIDGE_BUILD_TYPE="${OPT_BUILD_MODE}"
        export AIDGE_ASAN="${OPT_ASAN}"
        export AIDGE_NB_PROC="${nb_proc}"
        export AIDGE_INSTALL="${AIDGE_INSTALL_PREFIX}"
        export AIDGE_WITH_CUDA="${OPT_WITH_CUDA}"
        export AIDGE_WITH_PYBIND="${OPT_WITH_PYBIND}"
        export AIDGE_CMAKE_ARCH="${CMAKE_OSX_ARCH}"

        # If additional CMake arguments are provided, pass them
        local cmake_args=()
        if [[ -n "${POSITIONAL_ARGS[*]}" ]]; then
            cmake_args=(-D"AdditionalArgs=${POSITIONAL_ARGS[*]}")
        fi

        # Install requirements if present
        if [[ -f "aidge/${module}/requirements.txt" ]]; then
            python -m pip install -r "aidge/${module}/requirements.txt"
        fi

        # Build the Python module
        python -m pip install "aidge/${module}" -v

        # Run tests if enabled
        if [[ "$ENABLE_TESTS" == "ON" ]]; then
            python -m unittest discover -s "aidge/${module}/${module}/unit_tests/" -v
        fi
    fi
}

build_cpp_module() {
    local module="$1"
    local module_path="aidge/${module}"

    if [[ -f "${module_path}/CMakeLists.txt" ]]; then
        mkdir -p "${module_path}/build_bundle_cpp"

        cmake \
            -S "${module_path}" \
            -B "${module_path}/build_bundle_cpp" \
            -DCMAKE_INSTALL_PREFIX:PATH=${AIDGE_INSTALL_PREFIX} \
            -DCMAKE_C_COMPILER="${OPT_C_COMPILER}" \
            -DCMAKE_CXX_COMPILER="${OPT_CXX_COMPILER}" \
            -DCMAKE_BUILD_TYPE=${OPT_BUILD_MODE} \
            -DPYBIND=${OPT_WITH_PYBIND} \
            -DCUDA=${OPT_WITH_CUDA} \
            -DTEST=${ENABLE_TESTS} \
            -DENABLE_ASAN=${OPT_ASAN} \
            -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
            ${CMAKE_OSX_ARCH} \
            ${POSITIONAL_ARGS[@]}

        cd "${module_path}/build_bundle_cpp"

        make all install "-j${nb_proc}"

        [[ "$ENABLE_TESTS" == "ON" ]] && \
            ctest --output-on-failure || true

        cd - > /dev/null
    fi
}

build_aidge "$@"
param(
    [switch]$Python,
    [switch]$Pybind,
    [switch]$Tests,
    [switch]$Cuda,
    [switch]$Asan,
    [switch]$All,
    [switch]$Debug,
    [switch]$Release,
    [switch]$Help,
    [string[]]$Modules = @(),
    [int]$Cores = $env:NUMBER_OF_PROCESSORS - 1,
    [string]$Prefix = "$PSScriptRoot\aidge\install", # if ($env:AIDGE_INSTALL) { $env:AIDGE_INSTALL } else { (Get-Item .).FullName + '\aidge\install' }
    [string]$CC = "cl.exe",
    [string]$CXX = "cl.exe",
    [string[]]$PositionalArgs = @()
)

# Ensure we're using strict mode for better error handling
Set-StrictMode -Version Latest

# Colors and font options
$BOLD = [ConsoleColor]::DarkGray
$UNDERLINE = [ConsoleColor]::Cyan
$RED = [ConsoleColor]::Red
$GREEN = [ConsoleColor]::Green
$YELLOW = [ConsoleColor]::Yellow
$BLUE = [ConsoleColor]::Blue
$RESET = [ConsoleColor]::White

# Logging functions
function Write-Log {
    param(
        [string]$Level,
        [ConsoleColor]$Color,
        [string]$Message
    )
    $timestamp = Get-Date -Format "HH:mm:ss"
    Write-Host "[${Level}] - ${Message}" -ForegroundColor $Color
}

function Write-DebugLog { Write-Log -Level "DEBUG" -Color $GREEN -Message $args }
function Write-InfoLog { Write-Log -Level "INFO" -Color $BLUE -Message $args }
function Write-WarningLog { Write-Log -Level "WARNING" -Color $YELLOW -Message $args }
function Write-ErrorLog { Write-Log -Level "ERROR" -Color $RED -Message $args }

# Module dependency order
$OrderedModules = @(
    "aidge_base"
    "aidge_core",
    "aidge_backend_cpu",
    "aidge_backend_opencv",
    "aidge_backend_cuda",
    "aidge_onnx",
    "aidge_quantization",
    "aidge_export_cpp",
    "aidge_export_arm_cortexm",
    "aidge_learning"
)

# Default modules
$DefaultModules = @(
    "aidge_core",
    "aidge_backend_cpu",
    "aidge_onnx",
    "aidge_quantization",
    "aidge_export_cpp"
)

# Help message
$HelpMessage = @"
Aidge Build Script

SYNOPSIS
Builds the Aidge project with various configuration options.

DESCRIPTION
This script automates the building process for the Aidge project, allowing for
customized configuration through command-line parameters. It supports building
modules with Python bindings, running tests, enabling sanitizers, and more.

PARAMETERS
    -Python <Switch>        Build Python bindings.
    -Pybind <Switch>       Generate Python bindings.
    -Tests <Switch>        Build and run tests.
    -Cuda <Switch>         Build with CUDA support.
    -Asan <Switch>         Enable AddressSanitizer.
    -All <Switch>          Build all modules.
    -Debug <Switch>        Build in Debug mode (default).
    -Release <Switch>      Build in Release mode.
    -Help <Switch>         Show this help message.
    -Modules <String[]>    Modules to build (e.g., "core", "backend").
    -Cores <Int>           Number of cores to use for building (default: $Cores).
    -Prefix <String>       Installation prefix (default: $Prefix).
    -CC <String>           C compiler (default: $CC).
    -CXX <String>           C++ compiler (default: $CXX).

Detected OS: Windows

EXAMPLES
Build default modules with Python bindings:
./setup.ps1 -Python

Build all modules in Release mode:
./setup.ps1 -All -Release

Build specific modules with CUDA and tests:
./setup.ps1 -Modules "core", "backend_cuda" -Cuda -Tests

LOGGING
The script logs information to the console with different levels:
- DEBUG: Detailed debugging information.
- INFO: General information about the build process.
- WARNING: Non-critical issues that might affect the build.
- ERROR: Critical issues that prevent successful building.
"@

# Main build function
function Build-Aidge {
    # Determine build type
    if ($Release) {
        $BuildType = "Release"
    } else {
        $BuildType = "Debug"
    }

    # Determine modules to build
    if ($All) {
        $ModulesToBuild = Get-ChildItem -Path "aidge" -Directory |
            Where-Object { $_.Name -like "aidge_*" } |
            Select-Object -ExpandProperty Name
    }
    elseif ($Modules.Count -gt 0) {
        $ModulesToBuild = $Modules | ForEach-Object { "aidge_$_" }
    }
    else {
        $ModulesToBuild = $DefaultModules
        if ($Cuda) { $ModulesToBuild += "aidge_backend_cuda" }
    }

    # Build selected modules
    foreach ($module in $OrderedModules) {
        if ($ModulesToBuild -contains $module) {
            Write-InfoLog "Building module: $module"
            Build-Module -Module $module `
                -BuildType $BuildType `
                -WithPython:$Python `
                -WithPybind:$Pybind `
                -WithTests:$Tests `
                -WithCuda:$Cuda `
                -WithAsan:$Asan `
                -Cores $Cores `
                -PositionalArgs $PositionalArgs
        }
        else {
            Write-InfoLog "Skipping module: $module"
        }
    }
}

# Build module function
function Build-Module {
    param(
        [string]$Module,
        [string]$BuildType = "Debug",
        [bool]$WithPython = $false,
        [bool]$WithPybind = $false,
        [bool]$WithTests = $false,
        [bool]$WithCuda = $false,
        [bool]$WithAsan = $false,
        [int]$Cores = $env:NUMBER_OF_PROCESSORS - 1,
        [string[]]$PositionalArgs = @()
    )

    $modulePath = "aidge\$Module"

    if ($WithPython) {
        # Python module build
        if ($(Test-Path "$modulePath/setup.py") -or $(Test-Path "$modulePath/pyproject.toml")) {
            # Set environment variables for CMake
            $env:AIDGE_C_COMPILER = $CC
            $env:AIDGE_CXX_COMPILER = $CXX
            $env:AIDGE_BUILD_TYPE = $BuildType
            $env:AIDGE_ASAN = $WithAsan
            $env:AIDGE_NB_PROC = $Cores
            $env:AIDGE_INSTALL = $Prefix
            $env:AIDGE_WITH_CUDA = $WithCuda
            $env:AIDGE_WITH_PYBIND = $WithPybind

            # Handle additional CMake arguments
            # $cmakeArgs = if ($PositionalArgs.Count -gt 0) { "-DAdditionalArgs=$($PositionalArgs -join ' ')" } else { @() }

            # Install requirements if present
            if (Test-Path "$modulePath/requirements.txt") {
                python -m pip install -r "$modulePath/requirements.txt"
            }

            # Build the Python module
            python -m pip install "$modulePath" -v

            # Run tests if enabled
            if ($WithTests) {
                $testPath = "$modulePath/$module/unit_tests"
                if (Test-Path $testPath) {
                    python -m unittest discover -s $testPath -v
                }
            }
        }
    }
    else {
        # C++ module build
        $cmakeListPath = "$modulePath\CMakeLists.txt"
        if (Test-Path $cmakeListPath) {
            $buildDir = "$modulePath\build_bundle_cpp"
            New-Item -ItemType Directory -Force -Path $buildDir | Out-Null

            $pybindFlag = if ($WithPybind) { "ON" } else { "OFF" }
            $cudaFlag = if ($WithCuda) { "ON" } else { "OFF" }
            $testFlag = if ($WithTests) { "ON" } else { "OFF" }
            $asanFlag = if ($WithAsan) { "ON" } else { "OFF" }

            cmake -S "$modulePath" -B "$buildDir" `
                -DCMAKE_INSTALL_PREFIX:PATH="$Prefix" `
                -DCMAKE_C_COMPILER="$CC" `
                -DCMAKE_CXX_COMPILER="$CXX" `
                -DCMAKE_BUILD_TYPE="$BuildType" `
                -DPYBIND="$pybindFlag" `
                -DCUDA="$cudaFlag" `
                -DTEST="$testFlag" `
                -DENABLE_ASAN="$asanFlag" `
                -DCMAKE_EXPORT_COMPILE_COMMANDS=1 `
                @PositionalArgs

            cmake --build "$buildDir" --config "$BuildType" --parallel $Cores
            cmake --install "$buildDir" --config "$BuildType" -v

            if ($WithTests) {
                $originalLocation = Get-Location
                Set-Location "$buildDir/unit_tests"
                $ctestCommand = "ctest --output-on-failure"
                Invoke-Expression $ctestCommand -ErrorAction Continue
                Set-Location $originalLocation
            }
        }
        else {
            Write-ErrorLog "No CMakeLists.txt found in $modulePath"
        }
    }
}

# Show help and exit if help flag is provided
if ($Help) {
    Write-Host $HelpMessage
    exit
}

# Execute the build
Build-Aidge
#ifndef __AIDGE_EXPORT_CPP_KERNELS_SWISH__
#define __AIDGE_EXPORT_CPP_KERNELS_SWISH__

#include <type_traits>
#include <cmath>
#include "network/typedefs.hpp"
#include "network/utils.hpp"

template<int NB_OUTPUTS, 
         int OUTPUTS_HEIGHT, int OUTPUTS_WIDTH,
         typename Input_T, typename Output_T, typename Beta_T>
__attribute__((always_inline)) inline 
void swish_forward (
    const Input_T* inputs,
    Output_T* outputs,
    const Beta_T* betas)
{
    // To complete : Implement swish kernel
    // https://en.wikipedia.org/wiki/Swish_function
    for (size_t i = 0; i < NB_OUTPUTS; ++i) {
        for (size_t h = 0; h < OUTPUTS_HEIGHT; ++h) {
            for (size_t w = 0; w < OUTPUTS_WIDTH; ++w) {
            outputs[i*OUTPUTS_HEIGHT*OUTPUTS_WIDTH + h*OUTPUTS_WIDTH + w] = \
            inputs[i*OUTPUTS_HEIGHT*OUTPUTS_WIDTH + h*OUTPUTS_WIDTH + w] / \
            (1.0+exp((-1.0)*inputs[i*OUTPUTS_HEIGHT*OUTPUTS_WIDTH + h*OUTPUTS_WIDTH + w]*betas[i])) ;
            }
        }
    }
}

#endif  // __AIDGE_EXPORT_CPP_KERNELS_SWISH__
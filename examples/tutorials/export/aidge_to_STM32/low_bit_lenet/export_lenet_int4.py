import aidge_core
import aidge_backend_cpu
import aidge_core.export_utils
import aidge_onnx
import aidge_export_arm_cortexm
from aidge_quantization import quantize_network

from forward_dims import ArmFCDims, ArmConv2DDims
import numpy as np
import mnist
import os
import requests
import shutil


def propagate(model, scheduler, sample):
    # Setup the input
    # sample = np.reshape(sample, (1, 1, 28, 28))
    # input_tensor = aidge_core.Tensor(sample)
    # Run the inference
    scheduler.forward(data=[sample])
    # Gather the results
    output_node = model.get_output_nodes().pop()
    output_tensor = output_node.get_operator().get_output(0)
    return np.array(output_tensor)

def compute_accuracy(model, scheduler, samples, labels, divide_input=True, verbose=False):
    acc = 0
    for i in range(10000):
        
        x = np.reshape(samples[i], (1, 1, 28, 28)).astype('float32')
        if divide_input:
            x = aidge_core.Tensor(x/255)
        else:
            x = aidge_core.Tensor(x)
        if verbose:
            print(f"input tensor : {x}")
        y = propagate(model, scheduler, x)
        if verbose:
            print(f"output tensor : {y}")
            print(f"argmax output : {np.argmax(y)} - label {labels[i]}")
        if labels[i] == np.argmax(y):
            acc += 1
    return acc / len(samples)

# Load MNIST
mnist.temporary_dir = lambda: f'{os.path.abspath("")}'
mnist.datasets_url = "https://storage.googleapis.com/cvdf-datasets/mnist/"
test_images = mnist.test_images()
test_labels = mnist.test_labels()


# Create Aidge subset for PTQ
nb_calib = 100
calib_aidge_tensors = []
for i in range(nb_calib):
    t = aidge_core.Tensor((np.reshape(test_images[i], (1, 1, 28, 28)).astype('float32'))/255)
    calib_aidge_tensors.append(t)


# If LeNet ONNX file has not been downloaded yet
if not os.path.isfile("./lenet_mnist.onnx"):
    response = requests.get("https://huggingface.co/vtemplier/LeNet_MNIST/resolve/main/lenet_mnist.onnx?download=true")
    if response.status_code == 200:
        with open("lenet_mnist.onnx", 'wb') as f:
            f.write(response.content)
        print("ONNX model downloaded successfully.")
    else:
        print("Failed to download ONNX model. Status code:", response.status_code)


# Load ONNX model
model = aidge_onnx.load_onnx("lenet_mnist.onnx", verbose=True)

model.save("lenet_onnx")

# Expand MetaOps in the model
aidge_core.expand_metaops(model)

# Remove Flatten node, useless in the STM32 export
aidge_core.remove_flatten(model)

model.forward_dims([[1, 1, 28, 28]])
model.set_datatype(aidge_core.dtype.float32)
model.set_backend("cpu")

model.save("lenet_float32")

# Test Network before quantization
scheduler =  aidge_core.SequentialScheduler(model)
accuracy = compute_accuracy(model, scheduler, test_images, test_labels)
print(f'\n MODEL ACCURACY pre quantization : {accuracy * 100:.3f}%')

# Quantize model (via PTQ)
quantize_network(network = model, nb_bits = 4, input_dataset = calib_aidge_tensors, no_quantization=False, optimize_signs=True, verbose=True)


# Test "Fake Quantized" Network 
scheduler =  aidge_core.SequentialScheduler(model)
accuracy = compute_accuracy(model, scheduler, test_images, test_labels, divide_input=False, verbose=False)
print(f'\n MODEL ACCURACY post quantization : {accuracy * 100:.3f}%')


model.save("lenet_4bits_pre_fuse")


######### 1. Fuse to MetaOp
print("Fuse FcQuantizerReLU")
aidge_core.fuse_to_metaops(model, "FC->Quantizer->ReLU?", "ArmFC")
print("Fuse PadConvQuantizerReLU")
aidge_core.fuse_to_metaops(model, "Pad2D->Conv2D->Quantizer->ReLU?", "ArmPadConv2D")
print("Fuse ConvQuantizerReLU")
aidge_core.fuse_to_metaops(model, "Conv2D->Quantizer->ReLU?", "ArmConv2D")
print("Fuse succeeded !")

model.save("lenet4bits_Post_Fuse")

# Name MetaOperators 
node_ids = {
    "ArmFC": 0,
    "ArmPadConv2D": 0,
    "ArmConv2D": 0,
}

node_it = 0
for node in model.get_nodes():
    node_type = node.type()
    if node_type in node_ids and node.name() == "":
        node.set_name("_" + str(node_it) + "_" +str(node_type) + "_" + str(node_ids[node_type]))
        node_ids[node_type] += 1
        node_it += 1


# save the scaling_factor into attributes of the metaOp
op_list = ["ArmFC", "ArmPadConv2D", "ArmConv2D"]
for node in model.get_nodes():
    if node.type() in op_list:
        for mn in node.get_operator().get_micro_graph().get_nodes():
            if mn.type()=="Quantizer":
                for micro_node in mn.get_operator().get_micro_graph().get_nodes():
                    if micro_node.type()=="Mul":
                        scaling_factor = micro_node.get_parent(1).get_operator().get_output(0)
                        # Need to add then set attr for it to work
                        node.get_operator().attr.add_attr("scaling_factor", 0.0)
                        node.get_operator().attr.set_attr("scaling_factor", float(scaling_factor[0]))

######## 2. Transform MetaOperators to generic operators
to_gen_op_list = ["ArmFC", "ArmPadConv2D", "ArmConv2D"]
for node in model.get_nodes():
    if node.type() in to_gen_op_list:
        print(f"Transform {node.type()} {node.name()} to generic op") 
        aidge_core.to_generic_op(node)


######## 3. SetforwardDims of generic operators
map_forward_dims = {"ArmFC" : ArmFCDims, "ArmPadConv2D" : ArmConv2DDims, "ArmConv2D" : ArmConv2DDims}
for node in model.get_nodes():
    if node.type() in to_gen_op_list:
        print(f"SetforwardDims of generic op {node.type()} {node.name()}") 
        node.get_operator().set_forward_dims(map_forward_dims[node.type()](node).compute)


######## 4. Set datatypes on the "Fake Quantized" Network
apply_datatype_list = ["MaxPooling2D", "ArmFC", "ArmPadConv2D", "ArmConv2D"]
for node in model.get_nodes():
    if node.type() in apply_datatype_list:
        print(f"Set data type {node.type()} {node.name()}") 
        node.get_operator().set_datatype(aidge_core.dtype.dual_uint4)
        if node.get_operator().nb_inputs() > 1:
            #  weights datatype 
            node.get_parent(1).get_operator().set_datatype(aidge_core.dtype.int4)
            # Bias datatype
            node.get_parent(2).get_operator().set_datatype(aidge_core.dtype.int32)
    

#  Temporary fix - remove Clip input nodes
inputNodes = []
for n in model.get_ordered_inputs():
    print(n[0].name())
    print(n)
    if n[0].get_operator().input_category(n[1]) in [aidge_core.InputCategory(0), aidge_core.InputCategory(1)]: 
        inputNodes.append(n)

model.set_ordered_inputs(inputNodes)
print(model.get_input_nodes())

# Set datatype for input tensor (image MNIST on uint8)
# Fix - As there is no input tensor for the network, run forward dims to create one
model.forward_dims([[1, 1, 28, 28]])
model.get_ordered_inputs()[0][0].get_operator().get_input(0).set_datatype(aidge_core.dtype.uint8)

# Set datatype for output tensor (image MNIST on uint8)
model.get_ordered_outputs()[0][0].get_operator().get_output(0).set_datatype(aidge_core.dtype.int8)


model.save("lenet4bits_with_datatypes")

######## 5. Apply weight interleaving 
apply_WI_list = ["ArmPadConv2D", "ArmConv2D", "ArmFC"]

for node in model.get_nodes():
    if node.type() in apply_WI_list:
        print(f"Apply weight Interleaving for {node.type()} {node.name()}") 
        aidge_core.apply_weightinterleaving(node)


model.save("lenet4bits_Prepared_for_export")

######## Generate scheduling for export
model.forward_dims([[1, 1, 28, 28]])
# model.set_backend("cpu")
scheduler = aidge_core.SequentialScheduler(model)
scheduler.generate_scheduling()

export_folder = "export_lenet_int4"

if os.path.exists(export_folder) and os.path.isdir(export_folder):
    shutil.rmtree(export_folder)


######## Generate Export Library 
aidge_export_arm_cortexm.export(export_folder, 
                                model, 
                                scheduler, 
                                board ="stm32f7")

# # Generate inputs & associated labels for test app 
img = np.clip(test_images[0], 0, 255).astype(np.uint8).flatten()
aidge_core.export_utils.generate_input_file(export_folder="export_lenet_int4/data", 
                                     array_name="inputs",
                                     tensor=aidge_core.Tensor(img))
label = np.ones(1).astype(np.int8)
label[0] = test_labels[0]
aidge_core.export_utils.generate_input_file(export_folder="export_lenet_int4/data", 
                                     array_name="labels",
                                     tensor=aidge_core.Tensor(label))



import onnx
from onnx import helper
from onnx import TensorProto

filename = "test_swish.onnx"

onnx_inputs = [helper.make_tensor_value_info("data", TensorProto.FLOAT, [1, 10])]
onnx_outputs = [helper.make_tensor_value_info("swish_out", TensorProto.FLOAT, [1, 10])]
onnx_nodes = [helper.make_node(
    name="Swish0",
    op_type="Swish",
    inputs=["data"],
    outputs=["swish_out"],
)]
onnx_nodes[-1].attribute.append(helper.make_attribute("beta", [1.0]*10))

# Create the graph (GraphProto)
onnx_graph = onnx.helper.make_graph(
    nodes=onnx_nodes,
    initializer=[],
    name=filename,
    inputs=onnx_inputs,
    outputs=onnx_outputs,
)
# Create the model (ModelProto)
onnx_model = onnx.helper.make_model(
    onnx_graph,
    producer_name="aidge_onnx",
    producer_version="0.2.0"
)

onnx.save(onnx_model, filename)
